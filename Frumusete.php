<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<?php include "parts/head.php"?>

<body>
<div class="container-fluid" style="background-color: whitesmoke">
    <!--header-->
    <?php include "parts/header.php"?>
    <!-- Menu-->
    <?php include "parts/menu.php"?>
    <!--Continut-->
    <div class="container">
        <div class="row">
            <div class="col-9" align="center">
                <div align="center" width="80%">
        <h1>10 trucuri de folosire a mastii de par pe care nu ti le-a spus nimeni pana acum</h1>
        <img src="pictures/mascapar.JPG">
        <p>
            Parul nostru trece prin multe in fiecare zi. Agenti de curatare, caldura emanata de uscatorul de par, tortura cu aparatele de coafat, straturi de produse de styling, decolarare, vopsit… Si asta fara sa punem la socoteala factorii de mediu, precum poluarea, vantul puternic sau razele UV.

            Ei, bine, masca de par este un produs special creat pentru a combate toti acesti factori nocivi care ne afecteaza parul in fiecare zi. Si totusi, multe femei inca se feresc sa o foloseasca – poate pentru ca nu inteleg cum actioneaza sau cum ar trebui introdusa in rutina de ingrijire.

            Odata ce i-am inteles beneficiile, eu n-am mai putut trai fara ea. De fapt, avand par gros si uscat (parul ondulat tinde sa fie mai uscat decat cel drept) eu o folosesc de fiecare data cand imi spal parul, in loc de balsam. Multe dintre prietenele mele au fost scandalizate cand le-am povestit despre ritualul meu, mai ales ca stiau de regula aceea care spune ca masca pentru par se foloseste doar o data pe saptamana.

        <p style="color:red;"> 18 coafuri anti-plictiseala pentru parul lung</p>

            Adevarul e, insa, ca nu exista un singur mod de a o folosi. Trebuie mai intai sa experimentezi, sa cunosti nevoile parului tau si sa-i dai ceea ce are nevoie, cand are nevoie. Fara folosirea mastii la fiecare spalare, mi-ar fi foarte greu sa-mi descurc si sa-mi aranjez parul, dar in acelasi timp e adevarat ca acest lucru nu e valabil pentru toate tipurile de par.


</div></div>

                <div class="col-3">

                    <div style="border: 1px solid black"><div class="col-12"> <nav class="navbar navbar-expand-sm  ">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search">
                                <button class="btn btn-success" type="submit">Search</button> </div></div>
                    <div class="row">
                        <div class="col-12"></div><img src="pictures/meditatii.JPG"></div>

                </div>
            </div>
        </div></div>


    <!--footer-->

    <?php include "parts/footer.php"?>

</body></html>


