<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<?php include "parts/head.php"?>

<body>
<div class="container-fluid" style="background-color: whitesmoke">
    <!--header-->
    <?php include "parts/header.php"?>
    <!-- Menu-->
    <?php include "parts/menu.php"?>
    <!--Continut-->
    <div class="container">
        <div class="row">
            <div class="col-9" align="center">
<div align="center" width="80%">
    <h1> Cum trebuie sa arate sotia ideala in ochii unui barbat</h1>
    <img src="pictures/sotia.JPG"/>
    <p><h2>Noi, femeile, avem o viziune destul de clara despre ce inseamna sa fii o partenera buna.
        Cu toate acestea, barbatii vad lucrurile diferit.</h2>

    Sotia ideala, in ochii unui barbat, nu este la fel ca sotia ideala imaginata de femei.
    Reprezentantii sexului opus stiu exact ce isi doresc de la viitoarea lor nevasta si nu sunt deloc lucrurile la care te asteptai.
    </p>
    <p><u><b style="color: red">Calitatile feminine de care barbatii sunt atrasi instant</b></u><br/>
        Afla mai jos care sunt calitatile pe care barbatii le cauta cand vine vorba despre sotia ideala.
        Nu trebuie sa te mulezi pe ele, insa vei putea avea o perspectiva mai larga si mai complexa despre ce gandeste si ce isi doreste partenerul tau.</p>

        <br><b> 2. Sotia ideala se iubeste pe sine insasi</b></br>
    <p align="left"> <br align="left"><b>1. Sotia ideala pune pret pe familie </b></br>
        O femeie care isi iubeste familia si tine cont de ea are, in mod clar, potential de sotie.

        Barbatii percep femeile orientate spre viata de familie ca fiind serioase, mature si pregatite sa se implice intr-o relatie stabila.
        In plus, daca partenera isi iubeste propria familie si adora sa petreaca timp cu rudele,
        acest lucru le arata barbatilor ca potentiala sotie va fi deschisa si catre familia lui. Nimic nu este mai atragator decat o femeie sigura pe ea, care se accepta si se iubeste exact asa cum este. Aceste caracteristici dovedesc ca viitoarea sotie este un adult responsabil, curajos, hotarat si stapan pe propria viata.
        In plus, o femeie asumata este si o femeie naturala, careia nu ii este teama sa se comporte cu partenerul intr-un mod autentic. O partenera sigura pe sine nu va ezita niciodata sa isi faca aparitia in pijama, nemachiata si cu parul ravasit sau sa comande un burger in loc de salata, atunci cand doreste.
        <br><b>3. Sotia ideala ofera sprijin neconditionat</b></br>
        Nu degeaba se spune ca „un barbat de succes are intotdeauna alaturi o femeie puternica”. Orice barbat isi doreste sa aiba alaturi o femeie care sa il sustina, iar acest lucru este valabil si viceversa.
        Barbatii isi doresc ca viitoarele lor sotii sa le fie si prietene, confidente si sustinatoare. O femeie care are incredere in partenerul ei si il motiveaza sa devina cea mai buna versiune a lui va avea intotdeauna de castigat.De cele mai multe ori, partenerii care se sustin reciproc au relatii sanatoase, profunde si de lunga durata.
        <br><b>4. Sotia ideala nu uita sa fie feminina</b></br>
        Sa fii o femeie independenta si puternica este un lucru minunat. Cu toate acestea, barbatii tind sa se fereasca de partenerele mult prea dominante, care nu accepta niciodata ajutor, complimente sau gesturi galante.
        Barbatii adora sa isi faca partenera sa se simta speciala si protejata, iar acest lucru aduce beneficii pentru ambele parti. Femeile care primesc si apreciaza gesturile tandre sunt, fara indoiala, material de sotie.
        <br><b>5. Sotia ideala ofera afectiune si un sentiment de siguranta</b></br>
        Chiar daca barbatilor le place sa para puternici si duri, au nevoie de o partenera sensibila, iubitoare, care sa isi manifeste afectiunea in mai multe feluri.
        Barbatii adora ca potentiala sotie sa ii mangaie, sa ii tina in brate si sa ii alinte. In plus, ei apreciaza blandetea si intelegerea, atunci cand vine vorba despre partenera ideala.
    </p>


</div></div>
            <div class="col-3">

                <div style="border: 1px solid black"><div class="col-12"> <nav class="navbar navbar-expand-sm  ">
                            <input class="form-control mr-sm-2" type="text" placeholder="Search">
                            <button class="btn btn-success" type="submit">Search</button> </div></div>
                <div class="row">
                    <div class="col-12"></div><img src="pictures/meditatii.JPG"></div>

            </div>
        </div>
    </div></div>


    <!--footer-->

    <?php include "parts/footer.php"?>

</body></html>