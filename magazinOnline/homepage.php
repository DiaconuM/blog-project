<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
        <title>olx-pg anunt </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
              integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
<body >
<div class="container-fluid">   <!--Logo-->

    <div class="row">
        <div class="col-12">
            <nav class="navbar navbar-expand-sm bg-light navbar-dark">
                <!-- Brand/logo -->
                <a class="navbar-brand" href="#"><img src="picture/olx.jpg" class="rounded-circle" alt="Cinque Terre" width="80" height="50"/> </a>

                <!-- Links -->
                <ul class="nav ml-auto ">
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i style="font-size:24px; color: black;"
                                                        class="fa">&#xf2be;</i><font color="blue">Contul meu </font></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><p class="bg-warning text-white">+ ADAUGA ANUNT NOU</p></a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="container">
            <nav class="navbar navbar-expand-sm bg-primary navbar-dark">
                <form class="form-inline" action="">
                    <input class="form-control mr-sm-5" type="text" placeholder="4 123 425 anunturi in apropierea ta ">
                    <input class="form-control mr-sm-2" type="text" placeholder="Craiova, judetul Dolj">
                    <input class="form-control mr-sm-auto " STYLE="background-color:#000066" type="text" placeholder="CAUTA ACUM">

                </form>
            </nav>
            <div >
                <div class="row">
                    <div class="col-3"><img src="picture/car.JPG"/><a href="#"><b>Auto, moto si ambarcatiuni</b></a> </div>
                    <div class="col-3"><img src="picture/home.JPG"/><a href="#"> <b>Imobiliare</b></a></div>
                    <div class="col-3"><img src="picture/bag.JPG"/><a href="#"><b>Locuri de munca</b></a> </div>
                    <div class="col-3"><img src="picture/phone.JPG"/><a href="#"><b>Electrice si electorcasnice</b></a> </div>

                </div>
                <div class="row">
                    <div class="col-3"><img src="picture/coat.JPG"/><a href="#"><b>moda si frumusete </b></a> </div>
                    <div class="col-3"><img src="picture/sofa.JPG"/><a href="#"> <b>casa si gradina</b></a></div>
                    <div class="col-3"><img src="picture/mum.JPG"/><a href="#"><b>Mama si copilul</b></a> </div>
                    <div class="col-3"><img src="picture/bicicle.JPG"/><a href="#"><b>Sport,timp liber,arta</b></a> </div>

                </div>
                <div class="row">
                    <div class="col-3"><img src="picture/labuta.JPG"/><a href="#"><b>Animale de companie </b></a> </div>
                    <div class="col-3"><img src="picture/agro.JPG"/><a href="#"> <b>Agro si industrie</b></a></div>
                    <div class="col-3"><img src="picture/serv.JPG"/><a href="#"><b>Servicii afaceri echipamente firme</b></a> </div>
                    <div class="col-3"><a href="#"><b>Oferte de cazare</b></a> </div>

                </div>

            </div>
       <div >
           <div class="row"><h4>Anunturi promovate</h4></div>
           <div class="row">
               <div class="col-2">
                   <div class="row">
                       <img src="picture/incarcator.JPG" />
                   </div>
                   <div class="row">
                      <a href="#">Incarcator Samsung fast</a>
                   </div>
                   <div class="row">
                       <div class="col-6"> 49 lei</div>
                       <div class="fas fa-star "></div>
                   </div>
               </div>
               <div class="col-2">
                    <div class="row">
                        <img src="picture/teren.JPG" />
                    </div>
                   <div class="row">
                       <a href="#">Vand inchiriez teren</a>
                   </div>
                   <div class="row">
                       <div class="col-6"> 70 $</div>
                       <div class="col-6 fas fa-star"> </div>
                   </div>
               </div>
                   <div class="col-2">
                       <div class="row">
                           <img src="picture/navigatie.JPG" />
                       </div>
                       <div class="row">
                           <a href="#">Navigatie BMW</a>
                       </div>
                       <div class="row">
                           <div class="col-6">3 399 lei</div>
                           <div class="col-6 fas fa-star"></div>
                       </div>
                   </div>

                   <div class="col-2">
                       <div class="row">
                            <img src="picture/trasport.JPG" />
                       </div>
                       <div class="row">
                           <a href="#">Servicii Trasport marfa</a>
                       </div>
                       <div class="row">
                           <div class="col-6"></div>
                           <div class="col-6 fas fa-star">
                           </div>
                       </div>
                   </div>

                   <div class="col-2">
                       <div class="row">
                           <img src="picture/pisica.JPG" />
                       </div>
                       <div class="row">
                           <a href="#">DJ Evenimente</a>
                       </div>
                       <div class="row">
                           <div class="col-6"></div>
                           <div class="col-6 fas fa-star"></div>
                       </div>
                   </div>
               <div class="col-2">
                   <div class="row">
                       <img src="picture/pisica.JPG" />
                   </div>
                   <div class="row">
                       <a href="#">DJ Evenimente</a>
                   </div>
                   <div class="row">
                       <div class="col-6"></div>
                       <div class="col-6 fas fa-star"></div>
                   </div>
               </div>
           </div>
       </div>

    </div>













    </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>