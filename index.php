<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<?php include "parts/head.php"?>

<body>
<div class="container-fluid" style="background-color: whitesmoke">
    <!--header-->
  <?php include "parts/header.php"?>
        <!-- Menu-->
<?php include "parts/menu.php"?>
    <!--Continut-->
<div class="container">
    <div class="row">
        <div class="col-9" align="center">
            <div class="row"><div class="col-12" style="background-color:white"><h3>DEZVOLTARE PERSONALA</h3></div></div>
                    <div class="row"><div class="col-12"> <img src="pictures/sotia.JPG" width="100%" align="center"/></div></div>
                <div class="row" style="background:white"><div class="col-12"> <h5 class="text-center">Cum trebuie sa arate sotia ideala in ochii unui barbat</h5>
                        <p class="text-center">Noi, femeile, avem o viziune destul de clara despre ce inseamna sa fii o partenera buna.</p></div></div>


           <div class="row">
                <div class="col-6"> <img src="pictures/mascapar.JPG" class="img-fluid" width="120%" height="50%" /></div>
                <div class="col-6" style="background:white"><h5>10 trucuri de folosire a mastii de par pe care nu ti le-a spus nimeni pana acum</h5>
                    <p> Parul nostru trece prin multe in fiecare zi. Agenti de curatare, caldura emanata de uscatorul de par,
                        tortura cu aparatele de coafat, straturi de produse de styling, decolarare, vopsit… Si asta fara sa
                        punem la socoteala factorii de mediu, precum poluarea, vantul puternic sau razele UV...
                        <a href="frumusete.php"><u>citeste mai departe</u></a></p>
                </div>
           </div>
            <div class="row">
                <div class="col-6"> <img src="pictures/moda.JPG" width="120%"  class="img-fluid" height="50%"/></div>
                <div class="col-6"style="background:white"><h5>6 idei de tinute cu blugi de la Kate Middleton si Meghan Marklet</h5>
                    <p>Esti indragostita de stilul vestimentar al lui Kate Middleton si Meghan Markle? Urmaresti toate apartiiile lor,
                        ca sa te inspiri in alcatuirea garderobei tale? Descopera ce le propun stilistii in materie de tinute casual...
                        <a href="moda.php"><u>citeste mai departe</u></a></p>
                </div>
            </div>

      </div>
        <div class="col-3">

            <div style="border: 1px solid black"><div class="col-12"> <nav class="navbar navbar-expand-sm  ">
                        <input class="form-control mr-sm-2" type="text" placeholder="Search">
                          <button class="btn btn-success" type="submit">Search</button> </div></div>
                <div class="row">
                    <div class="col-12"></div><img src="pictures/meditatii.JPG"></div>

            </div>
        </div>
</div></div>
    <!--footer-->

    <?php include "parts/footer.php"?>

</body></html>
